# Redmine Notification Bot

### Build Docker Image
```
 docker build \
    --build-arg STATEFILE="statefile" \
    --tag redmine:bot \
    -f Dockerfile .
```

### Create Docker Container
```
docker run \
    -it \
    --net=host \
    -e REDMINE_TOKEN="xxx" \
    -e ROCKET_CHANNEL="Experiment" \
    -e ROCKET_TOKEN="xxx" \
    -e ROCKET_USER_ID="xxx" \
    -e STATEFILE="statefile" \
    -e REPORT_TIME_LIST="11:44" \
    -e TEXT_STYLE="text" \
    --name redmine-bot \
    redmine:bot
```

### Filtering Notifications

Please configure the env `INSIGNIFICANT_TOPICS`,
separate the topics with `;`.
