from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
    name='Redmine Bot',
    version='1.0',
    description='Informs about new issues from Redmine on Rocket.Chat, sends summary reports.',
    long_description=long_description,
    author='Jonas Schäfer, Lavon Śpirydonaŭ',
    author_email='jonas.schaefer@cloudandheat.com',
    packages=['Redmine Bot'],
    install_requires=['environ-config', 'python-redmine', 'tabulate']
)
