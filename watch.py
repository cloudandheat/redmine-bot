import base64
import email
import imaplib
import os
import re
import requests
import subprocess
from time import sleep
import tempfile
import environ
from datetime import datetime, timedelta, time, date
from redminelib import Redmine
from tabulate import tabulate, SEPARATING_LINE


@environ.config(prefix="")
class EnvConfig:

    def convert_str_to_time_list(str_to_convert):
        time_list = []
        if str_to_convert is not None and str_to_convert != '':
            time_list_str = str_to_convert.replace(' ', '').split(',')
            # Handle exceptions: time in range 00:00 to 23:59, 5 digits
            for t in time_list_str:
                if len(t) != 5:
                    raise Exception("The time format must match XX:YY")
                try:
                    time_list.append(time.fromisoformat(t)) 
                except:
                    raise Exception("The report time list is incorrect.")
        return time_list

    def convert_str_to_list(str_to_convert):
        return [e.strip() for e in str_to_convert.split(";") if e.strip() != '']

    insignificant_topics = environ.var("insignificant_topics", converter=convert_str_to_list)
    redmine_project = environ.var("customer-support")
    redmine_token = environ.var()
    redmine_url = environ.var("https://redmine.cloudandheat.com")
    redmine_status_id = environ.var("1")
    message_template = environ.var("@here :robot: Customer support ticket [{title}]({url}) was updated in {status} state")
    report_time_list = environ.var("11:00", converter=convert_str_to_time_list)    
    rocket_channel = environ.var()
    rocket_token = environ.var()
    rocket_url = environ.var("https://rocket.cloudandheat.com")
    rocket_user_id = environ.var()
    statefile = environ.var("statefile")
    text_style = environ.var("text_style")


def post_message_to_channel(user_id, token, channel, rocket_url, body):
    payload = {
        "channel": channel,
        "text": body,
    }
    print(payload)
    resp = requests.post(
        f"{rocket_url}/api/v1/chat.postMessage",
        json=payload,
        headers={
            "X-Auth-Token": token,
            "X-User-Id": user_id,
        },
    )
    resp.raise_for_status()


def fetch_new_issues(token, project, redmine_url, status_id):
    params = {
        "sort": "updated_on:desc",
    }
    if status_id is not None:
        params["status_id"] = status_id

    resp = requests.get(
        f"{redmine_url}/projects/{project}/issues.json",
        params=params,
        headers={
            "X-Redmine-API-Key": token,
        },
    )
    resp.raise_for_status()
    return resp.json()["issues"]


_LATEST_SEEN_CACHE = None


def set_latest_seen(value):
    global _LATEST_SEEN_CACHE
    if value is None:
        try:
            os.unlink(cfg.statefile)
        except FileNotFoundError:
            pass
    else:
        with open(cfg.statefile, "w") as f:
            f.write(value)
    _LATEST_SEEN_CACHE = value


def get_latest_seen():
    if _LATEST_SEEN_CACHE is not None:
        return _LATEST_SEEN_CACHE

    try:
        with open(cfg.statefile, "r") as f:
            return f.read() or None
    except FileNotFoundError:
        return None


def post_report(redmine_token, redmine_project, rocket_user_id, rocket_token, rocket_channel):

    redmine = Redmine(cfg.redmine_url, key = redmine_token)
    date_today = datetime.now().strftime("%Y-%m-%d")
    date_7_days_ago = (datetime.now() - timedelta(days=7)).strftime("%Y-%m-%d")

    new_issues_all_time = redmine.issue.filter(
        project_id = redmine_project,
        status_id = 1    # 1 = New, 2 = In Progress, 3 = Resolved, 4 = Feedback, 5 = Closed, 10 = Pending
    )

    in_progress_issues_all_time = redmine.issue.filter(
        project_id = cfg.redmine_project,
        status_id = 2
    )

    pending_issues_all_time = redmine.issue.filter(
        project_id = cfg.redmine_project,
        status_id = 10
    )

    issues_created_last_7_days = redmine.issue.filter(
        project_id = redmine_project,
        created_on =f'><{date_7_days_ago}|{date_today}',   # actually between 7 and 8 days
        status_id = '*'
    )

    new_issues_finished_last_7_days = issues_created_last_7_days.filter(
        status__name__in = ('Rejected', 'Closed', 'Resolved')
        )

    issues_finished_last_7_days = redmine.issue.filter(
        project_id = cfg.redmine_project,
        status_id = '*',
        updated_on =f'><{date_7_days_ago}|{date_today}',   # actually between 7 and 8 days
    ).filter(
        status__name__in = ('Rejected', 'Closed', 'Resolved')
    )
        
    text_message_as_html = f"""
Redmine :robot: Report
<table style="text-align:center">
<tbody>
<tr>
<th colspan=3 style="border-right-width:2px">At the moment</th>
<th colspan=3>Last 7 days</th>
</tr>

<tr>
<td rowspan=2 style="vertical-align: middle">New</td>
<td rowspan=2 style="vertical-align: middle">In Progress</td>
<td rowspan=2 style="vertical-align: middle; border-right-width:2px">Pending</td>
<td rowspan=2 style="vertical-align: middle">New</td>
<td colspan=2> Resolved / Rejected / Closed </td>
</tr>

<tr>
<td>from last 7 days</td>
<td>from any time</td>
</tr>

<tr>
<td>{len(new_issues_all_time)}</td>
<td>{len(in_progress_issues_all_time)}</td>
<td style="border-right-width:2px">{len(pending_issues_all_time)}</td>
<td>{len(issues_created_last_7_days)}</td>
<td>{len(new_issues_finished_last_7_days)}</td>
<td>{len(issues_finished_last_7_days)}</td>
</tr>

</tbody>
</table>
"""

    table_values = [('AT THE MOMENT', 'New', 'In Progress', 'Pending'),
                    ('', len(new_issues_all_time), len(in_progress_issues_all_time), len(pending_issues_all_time)),
                    SEPARATING_LINE,
                    ('LAST 7 DAYS', 'New', 'Resolved/Rejected/Closed\nFrom Last 7 Days', 'Resolved/Rejected/Closed\nFrom Any Time'),
                    ('', len(issues_created_last_7_days), len(new_issues_finished_last_7_days), len(issues_finished_last_7_days))]

    text_message_as_text = '```no\n ' + tabulate(table_values, tablefmt='simple') + '\n```'

    if 'html' in text_style.lower():
        post_message_to_channel(
        rocket_user_id,
        rocket_token,
        rocket_channel,
        cfg.rocket_url,
        text_message_as_html
        )
    else:
        post_message_to_channel(
        rocket_user_id,
        rocket_token,
        rocket_channel,
        cfg.rocket_url,
        text_message_as_text
        )

def is_insignificant(subject, insignificant_topics):
    for insignificant_topic in insignificant_topics:
        if re.search(insignificant_topic, subject):
            return True
    return False


if __name__ == "__main__":
    cfg = environ.to_config(EnvConfig, environ=os.environ)
    latest_seen = get_latest_seen()
    if latest_seen is None:
        issues = fetch_new_issues(cfg.redmine_token, cfg.redmine_project, cfg.redmine_url, cfg.redmine_status_id)
        # initial startup, send a fun message
        post_message_to_channel(
            cfg.rocket_user_id,
            cfg.rocket_token,
            cfg.rocket_channel,
            cfg.rocket_url,
            f"Hi, redmine watch coldbooting :robot:. Found {len(issues)} new "
            "issue(s).",
        )
        if issues:
            issues = sorted(issues, key=lambda x: x["updated_on"])
            set_latest_seen(issues[-1]["updated_on"])
    else:
        # post_message_to_channel(
        #     cfg.rocket_user_id,
        #     cfg.rocket_token,
        #     cfg.rocket_channel,
        #     cfg.rocket_url,
        #     f"Hi, redmine watch resuming :robot:. Will post about everything "
        #     f"after {latest_seen}",
        # )
        pass

    last_report_time = None    # Time in format datetime.time with second=0 and microsecond=0, or None

    while True:
        issues = fetch_new_issues(cfg.redmine_token, cfg.redmine_project, cfg.redmine_url, cfg.redmine_status_id)
        latest_seen = get_latest_seen()
        if latest_seen is None:
            updated_issues = issues
        else:
            updated_issues = list(filter(
                lambda x: x["updated_on"] > latest_seen,
                issues,
            ))
        if updated_issues:
            for issue in updated_issues:
                if not is_insignificant(issue['subject'], cfg.insignificant_topics):
                    post_message_to_channel(
                        cfg.rocket_user_id,
                        cfg.rocket_token,
                        cfg.rocket_channel,
                        cfg.rocket_url,
                        cfg.message_template.format(
                            title=issue["subject"],
                            url=f"{cfg.redmine_url}/issues/{issue['id']}",
                            status=issue["status"]["name"],
                        ),
                    )
            set_latest_seen(max(issue["updated_on"] for issue in issues))

        # summary report
        time_now = datetime.utcnow().time().replace(second=0, microsecond=0)
        if last_report_time != time_now:
        # we check if we need to report
            if time_now in cfg.report_time_list:
                # we need to make a report now
                post_report(cfg.redmine_token, cfg.redmine_project, cfg.rocket_user_id, cfg.rocket_token, cfg.rocket_channel)
                last_report_time = time_now
            else:
                # we did a report and the time has moved on enough,
                # set it (once more) to None (to allow this to work correctly when only a single reporting time is configured)
                last_report_time = None
        
        sleep(15)
