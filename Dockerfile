FROM python:3

ARG STATEFILE=statefile

ENV STATEFILE=$STATEFILE

RUN apt-get update && apt-get -y upgrade
RUN mkdir /etc/redmine-bot
RUN touch /etc/redmine-bot/${STATEFILE}

WORKDIR /etc/redmine-bot

COPY watch.py watch.py
COPY setup.py setup.py
COPY README.md README.md

RUN pip3 install .

ENTRYPOINT ["python3", "./watch.py"]
